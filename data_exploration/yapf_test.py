#!/usr/bin/python
#****************************************************************#
# ScriptName: test.py
# Author: @alibaba-inc.com
# Create Date: 2017-05-25 13:19
# Modify Author: @alibaba-inc.com
# Modify Date: 2017-05-25 13:19
# Function:
#***************************************************************#
x = {'a': 37, 'b': 42, 'c': 927}

y = 'hello ' 'world'
z = 'hello ' + 'world'
a = 'hello {}'.format('world')


class foo(object):
    def f(self):
        return 37 * -+2

    def g(self, x, y=42):
        return y

    def f(a):
        return 37 + -+a[42 - x:y**3]
